function ask_for_strategy(task)

global exp

if strcmp(exp.data.language, 'English')
    m = 'How did you solve this task? Can you describe in a few sentences?';
else
    m = 'Wie hast du diese Aufgabe bearbeitet? Kannst du es in weniges S�tzen beschreiben?';
end

if strcmp(task, 'idea')
    if strcmp(exp.data.language, 'English')
        m = 'This is the end of the experiment! What do you suspect it was about? (Please write a few sentences.)';
    else
        m = 'Hast du eine Vermutung, um was es bei diesem Experiment ging?';
    end
end

exp.STRATEGYdata{exp.STRATEGYdata_counter, 1} = task;
exp.STRATEGYdata{exp.STRATEGYdata_counter, 2} = exp.version;
exp.STRATEGYdata{exp.STRATEGYdata_counter, 3} = inputdlg(m, 'Question', 5);

exp.STRATEGYdata_counter = exp.STRATEGYdata_counter + 1;
