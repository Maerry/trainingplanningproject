function ask_demographics

global exp

%% Second window (personal info - only in session 1)
if exp.session == 1
    fields = {'Birthday (mmddyyyy): ', 'Gender (f or m): ', 'Handedness (r or l): '};
    subjectInfoEntryTitle = 'Subject Info Entry';
    num_lines = 1;
    answer = inputdlg(fields,subjectInfoEntryTitle,num_lines);
    exp.data.birthday       = (answer{1});
    exp.data.gender         = (answer{2});
    exp.data.handedness     = (answer{3});
end