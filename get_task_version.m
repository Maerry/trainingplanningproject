function get_task_version

%% Get stimuls versions
global exp;

% For Labyrinth & Association task: A, B, C, & D
load versions.mat                                                           % 'versions' contains four different versions for each subject

if     exp.session == 1 &&  exp.first_time
    exp.LABversion = versions(1, str2double(exp.subj));
elseif exp.session == 1 && ~exp.first_time
    exp.LABversion = versions(2, str2double(exp.subj));
elseif exp.session == 2 &&  exp.first_time
    exp.LABversion = versions(3, str2double(exp.subj));
elseif exp.session == 2 && ~exp.first_time
    exp.LABversion = versions(4, str2double(exp.subj));
end

% For Tower of London: A & B
if exp.first_time
    exp.TOLversion = datasample({'A', 'B'}, 1);
end

% For GoNogo: A & B
if isempty(exp.version)
    exp.versions = datasample({'A', 'B'}, 2, 'replace', false);
    exp.version = exp.versions{1};
else
    exp.version = exp.versions{2};
end