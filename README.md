# README #

This README documents what the code is about.

### What is this repository for? ###

* Make sure you download this folder AND SUBFOLDERS! Use git pull --recursive
* This repo contains the scripts to run the whole TrainingPlanning experiment and the data analysis scripts for the project
* The experiment code consists of several files: 'start_all_tasks.m' defines the functions that will start the whole experiment; the other functions are called inside 'start_all_tasks.m'.
* 'start_all_tasks.m' depends on several other projects. It starts the following tasks, which need to be available in the correct location on your machine: 
    * 2-step task ('/TwoStepTask')
    * Habit building task ('/FoodTask')
    * Tower of London ('/TowerOfLondonTask')
    * Category learning tasks ('/CategoryTask')
    * Control tasks ('/ControlTasks')
* The main analysis script is called 'Analyzing2stepTask.Rmd'; it reads in the data, analyzes it, and plots the results; the model part of what it reads in comes from Matlab scripts inside the TwoStepTask folder.

### Who do I talk to? ###

* Maria Eckstein (maria.eckstein@berkeley.edu)