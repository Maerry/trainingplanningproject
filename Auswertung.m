%% Navigate to the right folder

cd('C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\Training Planning')

%% Add mine & Klaus' Scripts
addpath('C:\Users\maria\Documents\MEGAsync\GSN\Wunderlich Lab\Training Planning')
addpath('C:\Users\maria\Documents\MEGAsync\GSN\Wunderlich Lab\Training Planning\2steptask\model\devel')

%% Navigation Association
% Save results as csv file
for lab_ass = {'lab', 'ass'}
    file_dir = 'C:\Users\maria\Documents\MEGAsync\GSN\Wunderlich Lab\Training Planning\NavigationAssociationTask\Results\';
    files = dir([file_dir, char(lab_ass)]);
    fileIndex = find(~[files.isdir]);

    ass = [];
    lab = [];

    for i = 1:length(fileIndex)
        fileName = files(fileIndex(i)).name;
        load(['NavigationAssociationTask\Results\' char(lab_ass) '\' fileName]);
        % Get subj ID, session, testtrial, ACC, RT, etc.
        testtrial = exp.LABdata.testtrial;
        ACC = exp.LABdata.ACC;
        RT = exp.LABdata.RT;
        pos = exp.LABdata.pos;
        stime = exp.LABdata.stime;
        key = exp.LABdata.key;
        keytime = exp.LABdata.keytime;
        forbidden_step = exp.LABdata.forbidden_step;
        
        subjID = repmat(str2double(exp.subj), length(ACC), 1);
        session = repmat(exp.session, length(ACC), 1);
        first_time = repmat(exp.first_time, length(ACC), 1);
        trial = 1:length(ACC);
        % Save them into a matrix
        subjdata = [subjID, session, first_time, trial', testtrial', ACC', RT', pos', stime', key', keytime', forbidden_step'];
        if strcmp(lab_ass, 'ass')
            ass = [ass; subjdata];
        else
            lab = [lab; subjdata];
        end
    end
    if strcmp(lab_ass, 'ass')
        csvwrite([file_dir '\Ass.csv'], ass);
    else
        csvwrite([file_dir '\Lab.csv'], lab);
    end        
end

%% GoNogo
% Save results as csv file
file_dir = 'C:\Users\maria\Documents\MEGAsync\GSN\Wunderlich Lab\Training Planning\GoNogo\Results\';
files = dir(file_dir);
fileIndex = find(~[files.isdir]);

GN = [];

for i = 1:length(fileIndex)
    fileName = files(fileIndex(i)).name;
    load([file_dir fileName]);
    % Get subj ID, session, testtrial, ACC, RT, etc.
    upper_case = exp.GNdata.upper;
    ACC = exp.GNdata.acc;
    RT = exp.GNdata.RT;
    key = exp.GNdata.key;
    time_start = exp.GNdata.tstart;
    block = exp.GNdata.block;

    subjID = repmat(str2double(exp.subj), length(ACC), 1);
    session = repmat(exp.session, length(ACC), 1);
    first_time = repmat(exp.first_time, length(ACC), 1);
    trial = 1:length(ACC);
    % Save them into a matrix
    subjdata = [subjID, session, first_time, trial', upper_case', ACC', RT', key', time_start', block'];
    GN = [GN; subjdata];
end
csvwrite([file_dir '\all\GN.csv'], GN);
