file_dir = "C:/Users/maria/MEGAsync/GSN/Wunderlich Lab/TrainingPlanningProject/BerkeleyData/TwoStep/ParameterEstimates"
files = list.files(file_dir, pattern = "_abk_")

allf = read.csv(paste(file_dir, files[1], sep = "/"), sep = ",", header = F, col.names = c("a1", "a2", "b1", "b2", "l", "p", "k", "w", "BIC", "SubjID", "Session", "Run", "f"), nrows = 0)

for (file in files) {
  f = read.table(paste(file_dir, file, sep = "/"), sep = ",", header = F, col.names = c("a1", "a2", "b1", "b2", "l", "p", "k", "w", "BIC", "SubjID", "Session", "Run", "f"))
  
  allf = rbind(allf, f)
}

write.csv(allf, paste(file_dir, "ga_fmincon_abk.csv", sep = "/"), row.names = F)
