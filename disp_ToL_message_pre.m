function disp_ToL_message_pre

global exp

%% Set up text
ToL_m1 = 'Your next task needs to be started by hand by an experimenter.';
ToL_m2 = 'Please raise your hand so that the experimenters see you are ready.';
ToL_m_ = ' ';
ToL_m3 = ['(Note for the experimenter: Please chose version ' exp.version '.)'];

%% Display message
msgbox({ToL_m1, ToL_m2, ToL_m_, ToL_m3});
disp(ToL_m3)