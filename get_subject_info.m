function get_subject_info

global exp;

%% makes prompt boxes
exp.data.language = 'English';
exp.STRATEGYdata = cell(10, 3);
exp.STRATEGYdata_counter = 1;

%% First window (date, time, condition, etc. - both sessions)
fields = {'Session (1 or 2): ', 'Subject number: ', 'Personal code: ', ...
    'Condition (leave blank):', 'Location (leave blank):'};
subjectInfoEntryTitle = 'Subject Info Entry';
num_lines = 1;
answer = inputdlg(fields, subjectInfoEntryTitle, num_lines);
exp.session         = str2double(char(answer{1}));
exp.subj            = (answer{2});                   
exp.data.initials   = (answer{3});
exp.location        = 'Xlab';
exp.data.date1      = 0;
exp.data.time1      = 0;
exp.data.date2      = 0;
exp.data.time2      = 0;

switch mod(str2double(char(exp.subj)), 5)
    case 0   % subjIDs 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, etc.
        exp.condition = 'co';
    case 1   % subjIDs 101, 106, 111, 116, 121, 126, 131, 136, 141, 146, 151, 156, etc.
        exp.condition = 'mbmb';
    case 2   % subjIDs 102, 107, 112, 117, 122, 127, 132, 137, 142, 147, 152, 157, etc.
        exp.condition = 'mbmf';
    case 3   % subjIDs 103, 108, 113, 118, 123, 128, 133, 138, 343, 148, 153, 158, etc.
        exp.condition = 'mfmf';
    case 4   % subjIDs 104, 109, 114, 119, 124, 129, 134, 139, 144, 149, 154, 159, etc.
        exp.condition = 'mfmb';
end

if ~isempty(answer{4})   % Condition (only there for debugging)
    exp.condition = answer{4};
end
if ~isempty(answer{5})   % Location (Xlab or other than Xlab? Other locations with specific file paths can be added)
    exp.location  = answer{5};
end
