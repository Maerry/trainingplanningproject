function disp_ToL_message_post(repetition)

global exp

ToL_bonus_m1 = 'Great job!';
ToL_m_ = ' ';
exp.bonus.task1_run1 = round((exp.bonus.max_session-1) / 8.1, 2);
exp.bonus.task1_run2 = round((exp.bonus.max_session)   / 8.1, 2);
if repetition == 1
    ToL_bonus_m2 = ['You earned a bonus of $' num2str(exp.bonus.task1_run1) ' in this task!'];
else
    ToL_bonus_m2 = ['You earned a bonus of $' num2str(exp.bonus.task1_run2) ' in this task!'];
end
ToL_bonus_m3 = 'Press ok to move on to the next task.';
msgbox({ToL_bonus_m1, ToL_bonus_m2, ToL_m_, ToL_bonus_m3})