function [rightID, initials, condition, language] = get_subj_ID_from_initials

global exp

initials = input('Initials: ', 's');

initial_counter = 0;
language = '';
rightIDs = {'0', '0', '0', '0', '0'};
rightIDs = [rightIDs, rightIDs, rightIDs, rightIDs, rightIDs];

for subjID = 1:100
    try
        load(['allResults/subj' num2str(subjID) 'session1.mat']);                       % Load subject data from session from for every subject (1:100)
%         disp(exp.data.initials)
        if strcmp(initials, exp.data.initials)                                          % If the initials are the same in the loaded data as the input,
            disp(['Subject "' initials '" had ID ' exp.subj ' in the first session (' num2str(exp.data.date1) ').']); % display the previous subj ID.
            language = exp.data.language;
            condition = exp.condition;
            initial_counter = initial_counter + 1;
            rightIDs{initial_counter} = num2str(exp.subj);
        else                                                                            % If the initials have not been recorded yet,
%             disp(['Subject ' exp.subj ' had different initials.']);                   % say so.
        end
        clear exp;
        
    catch
%         disp(['No data for subject ID ' num2str(subjID)]);
    end
    
end

if sum(~strcmp(rightIDs, '0')) == 1                                         % If exactly one previous subject had the given initials,
    rightID = rightIDs{1};                                                  % their ID is taken for the current subjID (because it MUST be the right one)
else                                                                        % If more than 1 previous subjects or none had the given initials,
    if sum(~strcmp(rightIDs, '0')) == 0                                     % If no previous subject had the given initials,
        disp('No previous subject had these initials.');                    % the user is told so.
    end
    
    rightID = input('Subject number: ', 's');                               % For both, the subjID needs to by typed in by hand,
    language = input('Preferred language (German / English): ', 's');       % and the language, too.
end