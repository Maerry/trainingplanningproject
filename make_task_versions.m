function make_task_versions

number_of_subj = 100;
versions = zeros(4, number_of_subj);

for column = 1:number_of_subj
    col = char(datasample({'A', 'B', 'C', 'D'}, 4, 'replace', false));
    versions(:,column) = col;
end

save('versions', 'versions')