function final_money_feedback

global exp

%% Prepare messages
m_ = ' ';
if exp.session == 1
    which_session = 'first';
    m4 = 'Thank you for coming in today and see you again in a couple of days!';
elseif exp.session == 2
    which_session = 'second';
    m4 = 'Thank you for taking part in this experiment! We appreciate your help!';
end

exp.total_bonus = exp.bonus.two_step1 + exp.bonus.two_step2 + ...
    exp.bonus.task1_run1 + exp.bonus.task1_run2 + ...
    exp.bonus.task2_run1 + exp.bonus.task2_run2;
exp.total_payment = exp.baseline_payment + exp.total_bonus;

m1 = ['You finished the ' which_session ' session of this experiment!'];
m2 = ['All your bonuses sum up to $' num2str(exp.total_bonus) ' today!'];
m3 = ['Your total payment today is $' num2str(exp.total_payment) '.'];

m5 = 'Please answer the questions that will appear after you close this window.';
m6 = 'We are working on your pay check in the meantime.';

%% Present messages
msgbox({m1, m_, m2, m3, m_, m_, m5, m6, m_, m_, m4}, 'Thank you!');
