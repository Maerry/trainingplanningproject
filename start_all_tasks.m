function start_all_tasks
  
% REMOVE COGENT PATH
% ADJUST FILE SAVE PATH?

%%
%%%%%%%%%%%%%%%%%%%%%%
%%% Set Parameters %%%
%%%%%%%%%%%%%%%%%%%%%%

global exp;

%% Get subject info & set variables
get_subject_info;

%% Set number of trials for each task
if strcmp(exp.subj, '0')
    exp.numb_of_trials.two_step = 10;                                      % 200
    exp.numb_of_trials.category = 10;                                      % 80 = 240/3 (calculated from my time; 3 sec per trial)
    exp.numb_of_trials.food     = 10;                                      % 60 = 240/4 (each trial takes 4 sec)
    exp.numb_of_trials.number   = 10;                                      % ???
    exp.numb_of_trials.rotation = 10;                                      % 70 = 240/3.4 (calculated from my time; 3.0-3.8 sec per trial)
    % How often do the subjects get summary feedback in each task? (i.e., "You got X out of the last Y trials right!")
    exp.number_of_summary_feedbacks = 2;
else
    exp.numb_of_trials.two_step = 200;                                     % 200
    exp.numb_of_trials.category = 90;                                      % 80 = 240/3 (calculated from my time; 3 sec per trial)
    exp.numb_of_trials.food     = 60;                                      % 60 = 240/4 (each trial takes 4 sec)
    exp.numb_of_trials.number   = 55;                                      % ???
    exp.numb_of_trials.rotation = 65;                                      % 70 = 240/3.4 (calculated from my time; 3.0-3.8 sec per trial)
    % How often do the subjects get summary feedback in each task? (i.e., "You got X out of the last Y trials right!")
    exp.number_of_summary_feedbacks = 5;
end

%% Add Cogent (MUST be an absolute path - relative paths do not stick after changing directory)
if strcmp(exp.location, 'Xlab')
    addpath(genpath('S:\eckstein\Cogent2000'))
else
    addpath(genpath('C:\TrainingPlanningProject\Cogent2000v1.33'))
    addpath(genpath('C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\Cogent2000v1.32'))
end

%%% Set task versions (determines which stimuli are presented; all tasks refer to the same exp.version (session 1: A B; session 2: C D)
%%% Also set money bonus (what is the maximum possible bonus per session?)
if exp.session == 1
    exp.version = 'A';
    exp.bonus.max_session = 9;
    exp.baseline_payment  = 5;
elseif exp.session == 2
    exp.version = 'C';
    exp.bonus.max_session = 16;
    exp.baseline_payment  = 10;
end

%%% Set filepaths (Xlab or other?)
if strcmp(exp.location, 'Xlab')
    exp.results_filepath = 'S:/eckstein/';
else
    exp.results_filepath = '';
end


%%
%%%%%%%%%%%%%%%%%
%%% Run Tasks %%%
%%%%%%%%%%%%%%%%%

%-----------%
%   Intro   %
%-----------%
pr_general_intro;

%----------------%
%   2-STEP PRE   %
%----------------%
cd('TwoStepTask')
% instructions
ts_initialize;
ts_start;
if exp.session == 1
    ts_instruction_flag = 2;
    exp.ts_try_quiz = 0;
    while ts_instruction_flag > 1.5
        ts_task_instructions;
        ts_instruction_flag = ts_instruction_quiz;
        exp.ts_try_quiz = exp.ts_try_quiz + 1;
    end
else
    ts_instruction_reminder;
end
% task
ts_task;
ts_close;
cd('..')


%-----------------%
%   MODEL-BASED   %
%-----------------%
if strcmp(exp.condition, 'mbmb') || ...
   strcmp(exp.condition, 'mbmf') && exp.session == 1 || ...
   strcmp(exp.condition, 'mfmb') && exp.session == 2

    for repetition = 1:2
        %%% ToL
        disp_ToL_message_pre
        pause(5)
        if strcmp(exp.location, 'Xlab')
            !"S:\eckstein\TowerOfLondonTask\TowerTasks.exe"
        else
            !"C:\eckstein\TowerOfLondonTask\TowerTasks.exe"
            !"C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\TowerOfLondonTask\TowerTasks.exe"
        end
        disp_ToL_message_post(repetition)
        pause(5)
        
        %%% Explicit (rule-based) category learning
        cd('CategoryTask')
        category_task('rb')
        cd('..')
        
        %%% Set version for second round
        if exp.session == 1
            exp.version = 'B';
        elseif exp.session == 2
            exp.version = 'D';
        end
        
    end

%----------------%
%   MODEL-FREE   %
%----------------%
elseif strcmp(exp.condition, 'mfmf') || ...
       strcmp(exp.condition, 'mfmb') && exp.session == 1 || ...
       strcmp(exp.condition, 'mbmf') && exp.session == 2
    
    for repetition = 1:2
        %%% Food Task
        cd('FoodTask')
        food_task2
        cd('..')

        %%% Implicit (information-integration) category learning
        cd('CategoryTask')
        category_task('ii')
        cd('..')
        
        %%% Set version for second round
        if exp.session == 1
            exp.version = 'B';
        elseif exp.session == 2
            exp.version = 'D';
        end
        
    end
    
%-------------%
%   Control   %
%-------------%
elseif strcmp(exp.condition, 'co')
    cd('ControlTasks')
    
    for repetition = 1:2
        %%% NumberComparison
        cd('NumberComparisonTask')
        Adults_NumComp
        cd('..')

        %%% OrientationDiscrimination
        cd('RotationTask')
        arrow_task
        cd('..')

        %%% Set version for second round
        if exp.session == 1
            exp.version = 'B';
        elseif exp.session == 2
            exp.version = 'D';
        end
    end
    
    cd('..')
 end

%-----------------%
%   2-STEP POST   %
%-----------------%
cd('TwoStepTask')
% instructions
ts_initialize;
ts_start;
ts_instruction_reminder;
% task
ts_task;
ts_close;
cd('..')


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Save data & payment file, say goodbye %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------%
%   Say goodbye   %
%-----------------%
final_money_feedback
disp(['total bonus: $' num2str(exp.total_bonus)])
disp(['total payment: $' num2str(exp.total_payment)])
pause(5)

%------------------------%
%   Write payment file   %
%------------------------%
fileID = fopen([exp.results_filepath, sprintf('Results/payment_subj%03s_sess%i.txt', exp.subj, exp.session)], 'w');
fprintf(fileID, '%s %s\r\n', 'subject', exp.subj);
fprintf(fileID,'%f\n', exp.total_payment);
fclose(fileID);

%-----------------------------%
%   Ask demographics & save   %
%-----------------------------%
ask_for_strategy('idea')
if exp.session == 1
    ask_demographics
end

result_file_name = [exp.results_filepath, sprintf('Results/ALLsubj%03s_sess%i.mat', exp.subj, exp.session)];
save(result_file_name, 'exp');
