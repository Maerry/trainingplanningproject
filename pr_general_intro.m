function pr_general_intro

global exp

config_display(1, 3, [1 1 1], [0.6 0.6 0.6], 'Helvetica', 30, 10, 0);               % Configure display (0 = window mode; 5 = 1280x1024; [1 1 1] = white background; grey text; fontname; fontsize; nbuffers)
config_keyboard;
start_cogent;

%% Prepare messages
if strcmp(exp.data.language, 'English')
    if exp.session == 1 
        m1 = 'Welcome and thank you for participating!';
        m2 = 'Let me briefly explain this experiment to you:';
        m3 = 'You will work on three different tasks, twice on each.';
        m4 = 'The computer will automatically guide you through the whole session.';
        m5 = '(Press space to move on.)';
        m6 = 'You will get instructions for each task on the computer. Please read carefully!';
        m7 = 'If something is unclear, raise your hand and an experimenter will help you.';
    else
        m1 = 'Welcome back and thank you again for participating!';
        m2 = 'Let me briefly explain what will happen today:';
        m3 = 'You will again work on three different tasks, twice on each.';
        m4 = 'Attention: Your tasks might be similar or different from last time!';
        m5 = '(Press space to move on.)';
        m6 = 'You will again get instructions for each task. Please read carefully!';
        m7 = 'If something is unclear, raise your hand and an experimenter will help you.';
    end
else
    m1 = 'Willkommen!';
    m2 = 'Ich werde dir kurz das Experiment erkl�ren:';
    m3 = 'Du wirst nacheinander sechs Aufgaben bekommen;';
    m4 = 'einige davon werden zweimal vorkommen.';
    m5 = '(Dr�cke die Leertaste, um fortzufahren.)';
    m6 = 'Bitte folge den Instruktionen und frage den Versuchsleiter, wenn irgendetwas unklar ist.';
    m7 = 'Lese bitte sorgf�ltig und gehe sicher, dass du die Aufgabe verstehst.';
end

m21 = ['You will receive a baseline payment of $' num2str(exp.baseline_payment) ' for your participation today.'];
m22 = 'However, you can increase your payment significantly by performing well';
m23 = 'and earning additional bonuses!';
m24 = ['All tasks together give bonuses of up to $' num2str(exp.bonus.max_session) ', distributed equally among the tasks.'];
m25 = 'All of your responses, right and wrong, are integrated in the calculation of the bonuses.';
m26 = 'Each trial counts!';
if exp.session == 1
    m27 = 'You will get a higher baseline payment as well as bigger bonuses in the second session!';
else
    m27 = '';
end

%% Present messages
% Slide 1
y = 200:-30:-200;
clearpict(1)
preparestring(m1, 1, 0, y(1))
preparestring(m5, 1, 0, y(14))
drawpict(1)
waitkeydown(inf, 71)

preparestring(m2, 1, 0, y(4))
preparestring(m3, 1, 0, y(6))
preparestring(m4, 1, 0, y(7))
drawpict(1)
waitkeydown(inf, 71)

preparestring(m6, 1, 0, y(9))  
drawpict(1)
waitkeydown(inf, 71)

preparestring(m7, 1, 0, y(11))
drawpict(1)
waitkeydown(inf, 71)

% Slide 2
clearpict(1)
preparestring(m21, 1, 0, y(1))
preparestring(m22, 1, 0, y(2))
preparestring(m23, 1, 0, y(3))
preparestring(m5,  1, 0, y(14))
drawpict(1)
waitkeydown(inf, 71)
preparestring(m24, 1, 0, y(6))
preparestring(m25, 1, 0, y(7))
preparestring(m26, 1, 0, y(10))
preparestring(m27, 1, 0, y(12))
drawpict(1)
waitkeydown(inf, 71)

stop_cogent