file_location = ... %where payment files are located
    'C:\Users\maria\MEGAsync\GSN\Wunderlich Lab\TrainingPlanningProject\Results\';

files = dir([file_location 'payment*.txt']); %get files matching pattern
file_names = {files.name}; %get only file names

subjects = cell(length(file_names),1);
money    = zeros(length(file_names),1);
row = 1;
for file_name = file_names
    subj = readtable([file_location file_name{1}], 'ReadVariableNames', false); % load each data file in the folder
    subjects{row} = char(subj{1,1});
    money(row) = str2double(char(subj{2,1}));
    row = row + 1;
end

payments = table(subjects, money);
writetable(payments, [file_location 'allpayments.csv']);
